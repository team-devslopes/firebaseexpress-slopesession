var express = require('express');
var router = express.Router();
var firebase = require('firebase');

firebase.initializeApp({
  /* You need to follow the setup procedures in the firebase server guide
     and download the file containing your service account key. That file
     should be referenced here.
  */
  serviceAccount: "<YOUR-SERVICE-ACCOUNT-KEY-FILE>",
  databaseURL: "<YOUR-FIREBASE-DB-URL>"
});

var db = firebase.database();
var ref = db.ref("data");
var techRef = db.ref("data/technologies");

var firebaseData = {};

ref.on("value", function(snapshot) {
  firebaseData = snapshot.val();
}, function(errorObject) {
  console.log("The read failed: " + errorObject.code);
});

/* GET /api/v1/data. */
router.get('/data', function(req, res) {
  res.send(firebaseData);
});

/* GET /api/v1/data/technologies */
router.get('/data/technologies', function(req, res) {
  res.send(firebaseData['technologies']);
});

/* POST /api/v1/data/technologies */
router.post('/data/technologies', function(req, res) {
  if (!req.body) return res.sendStatus(400);
  for (var key in req.body) {
    techRef.child(key).set(req.body[key], function(error) {
      if (error) {
        console.log("Data could not be saved. " + error);
      } else {
        console.log("Data saved successfully.");
      }
    });
  } res.sendStatus(200);
});

module.exports = router;
