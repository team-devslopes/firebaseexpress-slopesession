### Setup

Please follow along with the tutorial posted at
https://www.youtube.com/watch?v=HzrWQ0Y82RA. You
will need to install node.js as well as express-generator.

You will also need to download your Firebase service
account key file and be sure to specify it in routes/api.js.

Full details are in the video.
