//
//  DataService.swift
//  FirebaseExpress
//
//  Created by Jack Davis on 8/10/16.
//  Copyright © 2016 Devslopes. All rights reserved.
//

import Foundation
import Alamofire

protocol DataServiceDelegate: class {
    func dataLoaded()
}

class DataService {
    static let instance = DataService()
    
    weak var delegate: DataServiceDelegate?
    
    var technologyArray = [Technology]()
    
    func getTechApiData() {
        let url = NSURL(string: "http://localhost:3000/api/v1/data/technologies")
        Alamofire.request(.GET, url!).validate().responseJSON { response in
            if let result = response.result.value as? Dictionary<String, AnyObject> {
                print(result)
                self.technologyArray = Technology.loadTechnologyFromJSON(result)
                self.delegate?.dataLoaded()
            }
        }
    }
    
    func saveTechDataToFirebase(key: String, value: Bool) {
        let url = NSURL(string: "http://localhost:3000/api/v1/data/technologies")
        
        let parameters = [key: value]
        
        Alamofire.request(.POST, url!, parameters: parameters, encoding: .JSON)
            .validate()
            .responseString(completionHandler: { response in
                
                print(response)
        })
    }
}














