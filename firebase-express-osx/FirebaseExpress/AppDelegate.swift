//
//  AppDelegate.swift
//  FirebaseExpress
//
//  Created by Jack Davis on 8/10/16.
//  Copyright © 2016 Devslopes. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

