//
//  DataModel.swift
//  FirebaseExpress
//
//  Created by Jack Davis on 8/10/16.
//  Copyright © 2016 Devslopes. All rights reserved.
//

import Foundation

struct Technology {
    
    private var _key: String
    private var _value: Bool
    
    var key: String {
        return _key
    }
    
    var value: Bool {
        return _value
    }
    
    // init with parameters
    init(key: String, value: Bool) {
        _key = key
        _value = value
    }
    
    // init with Firebase data
    static func loadTechnologyFromJSON(json: Dictionary<String, AnyObject>) -> [Technology] {
        
        var technologyArray = [Technology]()
        
        for (key, value) in json {
            var id: String, val: Bool
            id = key
            if let value = value as? Bool {
                val = value
            } else {
                val = false
            }
            technologyArray.append(Technology(key: id, value: val))
        }
        return technologyArray
    }
}