//
//  MainVC.swift
//  FirebaseExpress
//
//  Created by Jack Davis on 8/10/16.
//  Copyright © 2016 Devslopes. All rights reserved.
//

import Cocoa

class DataEntryVC: NSViewController {
    
    @IBOutlet weak var keyTextField: NSTextField!
    private var _value: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func updateFirebaseButtonClicked(sender: NSButton) {
        if keyTextField.stringValue != "" {
            let key = keyTextField.stringValue

            print("Key: \(key), Value: \(_value)")
            DataService.instance.saveTechDataToFirebase(key, value: _value)
            DataService.instance.getTechApiData()
            keyTextField.stringValue = ""
            keyTextField.becomeFirstResponder()
        }
    }
    @IBAction func radioButtonChecked(sender: NSButton) {
        if sender.tag == 0 {
            _value = true
        } else {
            _value = false
        }
    }
}

