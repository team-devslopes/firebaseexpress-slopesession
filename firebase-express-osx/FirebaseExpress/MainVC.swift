//
//  MainVC.swift
//  FirebaseExpress
//
//  Created by Jack Davis on 8/10/16.
//  Copyright © 2016 Devslopes. All rights reserved.
//

import Cocoa

class MainVC: NSViewController {
    
    @IBOutlet weak var firebaseDataOutputLabel: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DataService.instance.delegate = self
        DataService.instance.getTechApiData()
        
        updateFirebaseDataLabel()
    }
    
    @IBAction func refreshDataButtonClicked(sender: NSButton) {
        DataService.instance.getTechApiData()
    }
    
    func updateFirebaseDataLabel() {
        firebaseDataOutputLabel.stringValue = "\(DataService.instance.technologyArray)"
    }
}

extension MainVC: DataServiceDelegate {
    func dataLoaded() {
        print("Data has finished loading")
        updateFirebaseDataLabel()
    }
}
